# Deploying a high-availability GitLab instance

TODO Add load balancer information

## GitLab architecture

GitLab mediates access to Git repositories. All client access goes
through NGINX (included in the omnibus packages) and OpenSSH (_not_
included in the omnibus packages). Persistent data is stored on
disk (a local filesystem or an NFS share), in a SQL database (Postgres
or MySQL), and in Redis.

On larger installations, SQL and Redis data make up less than 1%
of the persistent data stored on a GitLab server. The main question
when designing a highly available GitLab installation is how to
handle the other 99% of persistent data: the Git repositories.

### Git repository data storage

#### Floating block device

You can create an active/standby setup by using a 'floating' block
device such as a DRBD device, or a SAN volume from a storage appliance
or from your cloud provider (e.g. an Amazon EBS drive). A failover
then consists of attaching the block device to the new active server
and directing network traffic to that server.

#### Shared access block device

If your block device solution supports it you can attach the same
block device to multiple GitLab servers and give them shared access.
This gives you an active/active setup.  We do not see this a lot
because it requires special filesystems such as OCFS2.

#### NFS

This is a popular solution because NFS is a well-understood technology.
It allows you to run an active/active GitLab service (gitlab.com
has 16 application servers hooked up to one big NFS server at the
moment). From an availability standpoint, your GitLab service will
be as available as your NFS server.

See the [NFS documentation](doc/nfs.md) for more information about
configuring an NFS server for use with GitLab.

#### FUSE-mounted distributed filesystem

There seem to be a lot of 'distributed' filesystems that aim to
offer something similar to NFS, but with files distributed across
multiple storage servers, with the aim of higher redundancy and
easier storage scaling. For example, Gluster or LizardFS.

We do not know much about how such systems cope with the workload
provided by GitLab. On gitlab.com we are trying to move from an
Ubuntu NFS server to a LizardFS cluster. So far this has led to
challenges around file locking (flock(2) works on NFS but not on
LizardFS), which we are trying to address. The reason  for gitlab.com
to move away from NFS is that at our current total data size it is
hard to fit all data on a single 'cloud' NFS server.

We recommend to most of our customers to use a more established
technology such as NFS. At this time we cannot recommend any particular
non-NFS networked file system as a performant repository storage
solution for GitLab.
