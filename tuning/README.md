# Tuning your GitLab server

In this directory we are collecting GitLab server tuning tips.

## Do not let `locate` scan your git repositories

The Unix `locate` command uses an index maintained by `updatedb` from a cron
job. We recommend to uninstall locate or to configure it to avoid scanning
`/var/opt/gitlab` (omnibus-gitlab) or `/home/git` (source installations).
