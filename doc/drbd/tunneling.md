# IP tunneling for DRBD

When setting up DRBD synchronization between hosts that are not on the same
network one has to consider network routing and encryption. In this document we
will give an example configuration for setting up an IP tunnel with IPsec
encryption from AWS to a third-party data center.

In our example we have two hosts.

- `aws` has private IP 10.0.0.1 and public IP 1.2.3.4. The tunnel IP will be 172.16.228.1
- `offsite` has public IP 5.6.7.8. The tunnel IP will be 172.16.228.2

Both servers are running Ubuntu 12.04.

## IP tunneling

We will use the `ip` command provided by the `iproute` package. The tunnels we
create will not persist after a server reboot.

### On aws

```
# Create a tunnel device from aws to offsite
# Note that we use the private IP of aws as the local address
sudo ip tunnel add tunIPIP mode ipip local 10.0.0.1 remote 5.6.7.8

# Associate an IP address with the tunnel device
sudo ip address add 172.16.228.1/32 peer 172.16.228.2/32 dev tunIPIP

# Activate the tunnel device
sudo ip link set tunIPIP up
```

### On offsite

```
# Create a tunnel device from offsite to aws
# Note that we use the public IP of aws as the remote address
sudo ip tunnel add tunIPIP mode ipip local 5.6.7.8 remote 1.2.3.4

# Associate an IP address with the tunnel device
sudo ip address add 172.16.228.2/32 peer 172.16.228.1/32 dev tunIPIP

# Activate the tunnel device
sudo ip link set tunIPIP up
```

### Testing and troubleshooting

On both hosts, test the connection by pinging.

```
ping 172.16.228.1
ping 172.16.228.2
```

If something went wrong and you need to recreate a tunnel, delete it first.

```
sudo ip tunnel del tunIPIP
```

In some cases it can cause trouble to specify the local address of the tunnel.
If you leave it out, Linux will guess how to reach the remote.

```
# Example: do not specify the local address
sudo ip tunnel add tunIPIP mode ipip remote 5.6.7.8
```

## IPsec encryption

We will manually set up IPsec using `ipsec-tools`. This is OK for temporary
one-off tunnels, e.g. when migrating data via DRBD. For longer running setups
consider using Racoon which allows for things like automatic key rotation.

For the manual setup, we will need to share four keys between the two hosts:
two 128-bit random keys and two 192 bit random keys. We will denote these as
`key128-1`, `key128-2`, `key192-1` and `key192-2` below.

These steps are based on https://help.ubuntu.com/community/IPSecHowTo .

```
# Generate two 128-bit keys
# Example key: 0x03741b63ebb1b2ba87a1aa51397f652d
echo "key128-1: 0x$(openssl rand -hex 16)"
echo "key128-2: 0x$(openssl rand -hex 16)"

# Generate two 192-bit keys
# Example key: 0x5082048ba1187358cde52fe660d8cc7d02fd6917357771dc
echo "key192-1: 0x$(openssl rand -hex 24)"
echo "key192-2: 0x$(openssl rand -hex 24)"
```

### On aws

```
# Install ipsec-tools
sudo apt-get install ipsec-tools

# Create /etc/ipsec-tools.conf.
# THIS WILL OVERWRITE YOUR CURRENT IPSEC CONFIGURATION
(
umask 077
sudo tee /etc/ipsec-tools.conf <<'EOF'
#!/usr/sbin/setkey -f

# Configuration for 172.16.228.1

# Flush the SAD and SPD
flush;
spdflush;

# Attention: Use this keys only for testing purposes!
# Generate your own keys!

# AH SAs using 128 bit long keys
# Fill in your keys below!
add 172.16.228.1 172.16.228.2 ah 0x200 -A hmac-md5 key128-1;
add 172.16.228.2 172.16.228.1 ah 0x300 -A hmac-md5 key128-2;

# ESP SAs using 192 bit long keys (168 + 24 parity)
# Fill in your keys below!
add 172.16.228.1 172.16.228.2 esp 0x201 -E 3des-cbc key192-1;
add 172.16.228.2 172.16.228.1 esp 0x301 -E 3des-cbc key192-2;

# Security policies
# outbound traffic from 172.16.228.1 to 172.16.228.2
spdadd 172.16.228.1 172.16.228.2 any -P out ipsec esp/transport//require ah/transport//require;

# inbound traffic from 172.16.228.2 to 172.16.228.1
spdadd 172.16.228.2 172.16.228.1 any -P in ipsec esp/transport//require ah/transport//require;
EOF
)

# Protect the keys
sudo chmod 700 /etc/ipsec-tools.conf

# Enable IPsec
sudo service setkey start
```

### On offsite

```
# Install ipsec-tools
sudo apt-get install ipsec-tools

# Create /etc/ipsec-tools.conf.
# THIS WILL OVERWRITE YOUR CURRENT IPSEC CONFIGURATION
sudo tee /etc/ipsec-tools.conf <<'EOF'
#!/usr/sbin/setkey -f

# Configuration for 172.16.228.2

# Flush the SAD and SPD
flush;
spdflush;

# Attention: Use this keys only for testing purposes!
# Generate your own keys!

# AH SAs using 128 bit long keys
# Fill in your keys below!
add 172.16.228.1 172.16.228.2 ah 0x200 -A hmac-md5 key128-1;
add 172.16.228.2 172.16.228.1 ah 0x300 -A hmac-md5 key128-2;

# ESP SAs using 192 bit long keys (168 + 24 parity)
# Fill in your keys below!
add 172.16.228.1 172.16.228.2 esp 0x201 -E 3des-cbc key192-1;
add 172.16.228.2 172.16.228.1 esp 0x301 -E 3des-cbc key192-2;

# Security policies
# outbound traffic from 172.16.228.2 to 172.16.228.1
spdadd 172.16.228.2 172.16.228.1 any -P out ipsec esp/transport//require ah/transport//require;

# inbound traffic from 172.16.228.1 to 172.16.228.2
spdadd 172.16.228.1 172.16.228.2 any -P in ipsec esp/transport//require ah/transport//require;
EOF

# Protect the keys
chmod 600 /etc/ipsec-tools.conf

# Enable IPsec
sudo service setkey start
```

You are now running encrypted traffic between 172.16.228.1 and 172.16.228.2.

## Further steps

Set up DRBD to use the 172.16.228.x addresses. Do not forget to open your DRBD
port in the firewall if necessary.
