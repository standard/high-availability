# RAID striping on AWS EBS with LVM

In this document we will describe how we set up a striped logical volume to
hold git repository data for GitLab on Amazon EC2 using two 1TB EBS volumes.

## Background

Amazon's EBS 'virtual hard drives' are limited in size to 1 TB. If you expect
to need more storage for the Git repositories on your GitLab server we
recommend creating a striped logical volume with LVM.

Our example uses an EC2 instance running Ubuntu 12.04 64-bit with two 1TB
drives attached as `/dev/xvdf` and `/dev/xvdg` respectively.

## Dependencies

We need the `lvm2` package installed.

```shell
sudo apt-get install lvm2
```

## Creating the striped volume

First we check the list of attached devices.

```
ubuntu@ip-10-186-8-209:~$ lsblk
NAME  MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
xvda1 202:1    0     8G  0 disk /
xvdb  202:16   0  15.3G  0 disk /mnt
xvdf  202:80   0     1T  0 disk 
xvdg  202:96   0     1T  0 disk 
```

Now we create a volume group consisting of our two 1TB EBS drives. This is a
destructive operation.

```
ubuntu@ip-10-186-8-209:~$ sudo vgcreate vg0 /dev/xvdf /dev/xvdg
  No physical volume label read from /dev/xvdf
  Physical volume "/dev/xvdf" successfully created
  No physical volume label read from /dev/xvdg
  Physical volume "/dev/xvdg" successfully created
  Volume group "vg0" successfully created
```

We create a logical volume striped across two disks, using all available space.

```
ubuntu@ip-10-186-8-209:~$ sudo lvcreate --name gitlab_data --stripes 2 --extents 100%VG vg0
  Using default stripesize 64.00 KiB
  Logical volume "gitlab_data" created
ubuntu@ip-10-186-8-209:~$ lsblk
NAME                     MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
xvda1                    202:1    0     8G  0 disk /
xvdb                     202:16   0  15.3G  0 disk /mnt
xvdf                     202:80   0     1T  0 disk 
└─vg0-gitlab_data (dm-0) 252:0    0     2T  0 lvm  
xvdg                     202:96   0     1T  0 disk 
└─vg0-gitlab_data (dm-0) 252:0    0     2T  0 lvm  
```

With the volume created all we need to do now is create a filesystem and mount
it.

```
ubuntu@ip-10-186-8-209:~$ sudo mkfs.ext4 /dev/mapper/vg0-gitlab_data 
mke2fs 1.42 (29-Nov-2011)
Filesystem label=
OS type: Linux
Block size=4096 (log=2)
Fragment size=4096 (log=2)
Stride=16 blocks, Stripe width=32 blocks
134217728 inodes, 536868864 blocks
26843443 blocks (5.00%) reserved for the super user
First data block=0
Maximum filesystem blocks=4294967296
16384 block groups
32768 blocks per group, 32768 fragments per group
8192 inodes per group
Superblock backups stored on blocks: 
        32768, 98304, 163840, 229376, 294912, 819200, 884736, 1605632, 2654208, 
        4096000, 7962624, 11239424, 20480000, 23887872, 71663616, 78675968, 
        102400000, 214990848, 512000000

Allocating group tables: done                            
Writing inode tables: done                            
Creating journal (32768 blocks): done
Writing superblocks and filesystem accounting information: done       

ubuntu@ip-10-186-8-209:~$ sudo mkdir -p /mnt/gitlab_data
ubuntu@ip-10-186-8-209:~$ sudo mount /dev/mapper/vg0-gitlab_data /mnt/gitlab_data
```

Now we have a filesystem where we can let GitLab store its repositories and
satellites.

```
ubuntu@ip-10-186-8-209:~$ df -h
Filesystem                   Size  Used Avail Use% Mounted on
/dev/xvda1                   7.9G  898M  6.6G  12% /
udev                         1.9G  8.0K  1.9G   1% /dev
tmpfs                        751M  176K  750M   1% /run
none                         5.0M     0  5.0M   0% /run/lock
none                         1.9G     0  1.9G   0% /run/shm
/dev/xvdb                     16G  167M   15G   2% /mnt
/dev/mapper/vg0-gitlab_data  2.0T  199M  1.9T   1% /mnt/gitlab_data
```
