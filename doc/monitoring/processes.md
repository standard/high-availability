# Monitoring GitLab processes

A GitLab installation depends on a number of long-running processes:

- Nginx / Apache
- Postgres / MySQL
- Redis
- Sidekiq (GitLab Rails application web server)
- Unicorn (GitLab background job processor)

In this document we will only discuss the GitLab-specific processes.

## Omnibus-gitlab

Omnibus-gitlab ships with its own process supervision tree, managed by
[Runit](http://smarden.org/runit/index.html). Commands such as `gitlab-ctl
start nginx` get proxied to Runit. For more information about the capabilities
of Runit, see the documentation for [sv](http://smarden.org/runit/sv.8.html)
(used internally by gitlab-ctl to issue commands to services) and
[runsv](http://smarden.org/runit/runsv.8.html) (which supervises individual
services).

Each Runit service is configured via a 'service directory' in
`/opt/gitlab/service`. If you want to find the PID of e.g. PostgreSQL in a
script you can inspect `/opt/gitlab/service/postgresql/supervise/pid`:

```
$ sudo cat /opt/gitlab/service/postgresql/supervise/pid
8279
```

Please take note that Unicorn's PID cannot be retrieved through Runit because
it runs as a daemon process. The default path of the Unicorn pidfile is
`/opt/gitlab/var/unicorn/unicorn.pid`.

Also see [our Sidekiq Munin
plugin](https://gitlab.com/standard/cookbook-omnibus-gitlab/blob/master/files/default/omnibus_gitlab_sidekiq_rss)
as an example of how to monitor a process managed by Runit.

## Unicorn

Unicorn has built-in process supervision: a master process manages a fixed
number of worker processes. The number of workers is configured in [gitlab.rb
for
omnibus-gitlab](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/README.md#adjusting-unicorn-settings)
and in `/home/git/gitlab/config/unicorn.rb` for installastions from source. The
minimum number of workers is 2.

Due to the robustness of Unicorn's internal supervision we currently do not
monitor it on GitLab Cloud.

### Memory

Each worker picks a random memory limit between 200-250 MB for itself when it
is spawned. When the memory limit is exceeded, the worker will exit and the
master will spawn a replacement worker. This keeps Unicorn's memory use in check.

### CPU

Unicorn workers have a fixed request timeout (configured in `unicorn.rb`) after
which the master will kill a worker. This should ensure that high Unicorn CPU
loads are transient.

### Uptime

In our experience the Unicorn master process is very robust, and it will
respawn any crashed workers.

On installations from source (see above for omnibus-gitlab) you can monitor the
master via `/home/git/gitlab/tmp/pids/unicorn.pid` and start/stop it using the
commands below. On GitLab Cloud we are currently not monitoring Unicorn uptime
though.

```shell
sudo -u git -H /home/git/gitlab/script/web stop
sudo -u git -H /home/git/gitlab/script/web start
```

## Sidekiq

Sidekiq consists of a single process with 25 worker threads. As of the time of
writing (GitLab 6.7), Sidekiq is not very stable on GitLab Cloud. Not all
GitLab installations experience the same usage patterns and load as GitLab
Cloud however, so your mileage may vary.

### Memory

A fresh Sidekiq process for Ruby 2.0 and GitLab 6.7 starts out well below 200
MB RSS. On GitLab Cloud, we then see it grow slowly until our Monit setup
restarts it when the RSS exceeds 500MB. We hear few complaints about this
behavior from other GitLab administrators, but nevertheless we recommend to
monitor the Sidekiq process (`/home/git/gitlab/tmp/pids/sidekiq.pid`) and
restart Sidekiq when its memory exceeds 500 MB. You can restart Sidekiq without
touching the rest of GitLab using:

```shell
# omnibus-gitlab
sudo gitlab-ctl restart sidekiq

# installations from source
sudo -u git -H /home/git/gitlab/script/background_jobs restart
```

Because Sidekiq only does asynchronous work, the worst your users can expect
from a Sidekiq restart is an email being sent or a webhook being notified a
minute later than normal.

### CPU / load

In normal conditions, Sidekiq is idling most of the time with very brief bursts
of activity. If either of the following happens we recommend you to to restart
Sidekiq:

- Sidekiq maxes out its CPU core for more than 2 minutes;
- Sidekiq reports all workers busy (`/home/git/gitlab/script/background_jobs load_ok`
  returns non-zero).

Our [(no longer maintained) monit
configuration](https://gitlab.com/gitlab-org/cookbook-gitlab/blob/master/templates/default/sidekiq.monitrc.erb)
does these checks.

### Uptime

With omnibus-gitlab installations, Sidekiq will be restarted by Runit if necessary.

With installations from source, Sidekiq cannot recover on its own from crashes,
so if the PID in `/home/git/gitlab/tmp/pids/sidekiq.pid` is not up you should
start Sidekiq again with:

```shell
sudo -u git -H /home/git/gitlab/script/background_jobs start
```

Our monit configuration does this automatically.
