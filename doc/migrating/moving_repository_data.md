# Moving GitLab repository data to a different server

GitLab Cloud started out storing its repositories on a single Amazon EBS drive.
When we reached the limits of EBS volumes (1TB) we needed to move to a larger
storage volume; in our case an [LVM logical
volume](doc/lvm/raid_striping_on_aws_ebs_with_lvm.md). We tried three different
approaches to moving our repository data to the new volume: in-place conversion
to LVM, rsyncing to a new LVM drive and creating a slave copy on a remote LVM
volume using DRBD.

## Converting the existing filesystem to a logical volume in a volume group in-place

We looked into converting the existing filesystem in-place with
[blocks](https://github.com/g2p/blocks). This would mean unmounting the
filesystem, freeing up space on the block device by shrinking the filesystem,
and then writing the required LVM metadata into the freed space.

We chose not to use this approach because the conversion software was hard to
install on Ubuntu 12.04 and the amount of downtime in our situation appeared to
exceed one hour.

## Using Rsync to move repository data to the new storage volume

This approach works as follows. Suppose the existing repository data is in
`/home/git/repositories` and our new storage volume is `/mnt/gitlab_data`. We
would then do repeated Rsync runs from `/home/git/repositories` to
`/mnt/gitlab_data`:

```shell
# Use `nice` to not interfere with users using GitLab.
# Use `--hard-links` to preserve Git deduplication for e.g. forks.
sudo nice rsync --archive --hard-links --verbose --verbose --delete /home/git/repositories /mnt/gitlab_data/
sudo ionice -c 3 -p $(pgrep rsync) # reduce the IO priority of all rsync processes
```

The initial Rsync run needs to copy a lot of data; the time required depends on
the amount of repository data and the IO characteristics of your server. Repeat
Rsync runs will take shorter and shorter until they reach the point where Rsync
is mostly looking for changes in `/home/git/repositories`. When you reach this
point, you need to decide if the minimum Rsync time is acceptable as downtime
for your server. We decided against it because it would have required several
hours of downtime for our data / infrastructure (1TB of repositories on an
Amazon EBS volume).

If the minimum rsync time is acceptable for you, you can do the actual
migration with the following steps:

- perform the penultimate Rsync run while GitLab is up;
- shut down GitLab;
- wait for all active `git` user processes to terminate or run `sudo pkill -u
  git`;
- perform the last Rsync run now that GitLab is down;
- edit the gitlab-shell and gitlab configuration files to use the new
  repository and satellite paths on `/mnt/gitlab_data`;
- start GitLab;
- recreate the GitLab satellites.

Merge requests and the web editor will not be fully functional until all the
satellites are rebuilt.

## Replicating the repository filesystem using DRBD

The solution we used in the end on GitLab Cloud was to provision a new AWS
instance with a larger attached storage volume attached (two EBS volumes pooled
into one LVM volume group). We then temporarily set up DRBD disk replication
from the repository drive on the active server to the LVM drive on the new
server. Once the slave on the new server was up to date, we did a failover from
the old server to the new one during which we disabled DRBD and resized the
filesystem on the slave volume. The end result was a new server holding a
resized version of the existing repository filesystem on an LVM logical volume,
at the expense of two times 15 minutes downtime.

### Install DRBD

DRBD consists of a kernel module and a collection of userland tools. First we
check if the kernel module is already installed.

```shell
# BOTH
# Load the drbd module and inspect its version
sudo modprobe drbd && cat /proc/drbd
```

You might have to install additional kernel modules. On AWS with Ubuntu 12.04,
we had Linux 3.2.0-58 so we needed the following package.

```
sudo apt-get install linux-image-extra-3.2.0-58-virtual
# the following command should now succeed:
sudo modprobe drbd && cat /proc/drbd
```

Next, we install the userland tools.

```shell
# BOTH
sudo apt-get install drbd8-utils
# Look for version mismatch error messages
sudo drbdadm --version
```

If the `drbdadm` command complains about a version mismatch, you need to remove
the userland tools and install them from source.

```shell
# BOTH
sudo apt-get remove drbd8-utils

# install dependencies
sudo apt-get install autoconf gcc flex make git-core xsltproc # xsltproc is for the manpages
 
# check the DRBD kernel module version
cat /proc/drbd
 
# clone
cd /tmp && git clone -b drbd-8.4.2 git://git.drbd.org/drbd-8.4.git # assuming kernel module for DRBD 8.4.2
 
# compile
cd drbd-8.4
./autogen.sh
./configure --prefix=/usr --localstatedir=/var --sysconfdir=/etc
make
make doc
 
# install
sudo make install
```

### Prepare for the synchronization

On the new server, create the new,larger LVM logical volume that we are moving
to and an external metadata volume for DRBD. On GitLab Cloud, we moved from a
1TB volume to a 1.8TB volume (90% of 2TB) with an additional 80GB volume for
metadata on the new server. The 80GB was generously alotted based on
http://www.drbd.org/users-guide-8.3/ch-internals.html#s-meta-data-size .

On the old server, we attached a new 40GB EBS volume to store DRBD metadata for
the 1TB volume.

We used the following DRBD configuration file to synchronize the data; stored
in `/etc/drbd.d/gitlab_data.res`.

```
resource gitlab_data {
  protocol A; # async replication to reduce the performance impact on the master

  syncer {
    rate 7M; # max throughput when testing on AWS was 22MB; this is ~ 30% as recommended by DRBD http://www.drbd.org/users-guide-8.3/s-configure-syncer-rate.html
  }

  # master / old server
  on ip-10-147-29-194 { # should match `uname -n`
    device /dev/drbd0;  # device name of the new DRBD block device
    disk /dev/xvdh;     # this is the EBS drive with the original data
    flexible-meta-disk /dev/xvdg; # new EBS drive; 40GB
    address 10.147.29.194:7789; # internal IP of the master. add an inbound rule in the security group for port 7789
  }

  # slave / new server
  on ip-10-113-153-227 { # should match `uname -n`
    device /dev/drbd0;
    disk /dev/vg0/gitlab_data; # on the slave DRBD is backed by an LVM logical volume
    flexible-meta-disk /dev/vg0/drbd_md; # this metadata disk is 80GB
    address 10.113.153.227:7789;
  }
}
```

On the new server, we turn on DRBD in preparation for the synchronization.
Turning on DRBD on the old server will require downtime, see the next step.

```
# NEW
# initialize the metadata disk for the new DRBD volume
sudo drbdadm create-md gitlab_data

# start up DRBD for the gitlab_data resource
sudo drbdadm up gitlab_data
```

At this point, DRBD is running in a degraded state on the new server:

```
$ cat /proc/drbd # NEW
version: 8.3.11 (api:88/proto:86-96)
srcversion: 93CE421BB73A731BDC72D8E 
 0: cs:WFConnection ro:Secondary/Unknown ds:Inconsistent/DUnknown A r-----
    ns:0 nr:0 dw:0 dr:0 al:0 bm:0 lo:0 pe:0 ua:0 ap:0 ep:1 wo:f oos:1932726016
```

Make sure to test the network connection to the new server and back, e.g. using
telnet.

```
telnet 10.113.153.227 7789
```

### Start the synchronization

With the slave server ready and DRBD installed on the old server we can now
start the synchronization. To do this we need to remount the filesystem holding
the repository data, so we need to schedule maintenance for our GitLab
instance. On our AWS instance, we had our repository data mounted at `/mnt/ebs`
from an EBS drive attached at `/dev/xvdh`.

On the _old_ server, we did the following steps during our maintenance window.

```
# OLD

# Shut down gitlab
sudo service gitlab stop

# Unmount the data drive. If unmounting fails, run `sudo fuser -km /mnt/ebs` to
# forcefully kill all processes using the filesystem and try again.
sudo umount /mnt/ebs;

# Create an EBS snapshot in the AWS console

# Initialize the metadata device
sudo drbdadm create-md gitlab_data 

# Enable /dev/drbd0 and connect over the network
sudo drbdadm up gitlab_data

# Check for cs:Connected
cat /proc/drbd

# Destroy all data on the slave DRBD device. Only run this on the OLD server!
drbdadm -- --overwrite-data-of-peer primary gitlab_data

# Mount the DRBD device with our filesystem on it
sudo mount /dev/drbd0 /mnt/ebs

# Start GitLab
sudo service start gitlab

# Done; synchronization is underway!
```

With our setup, it took 40 hours for DRBD on the slave to catch up with the
master. You can check its progress on either node by running:

```
watch cat /proc/drbd
```

### Failover to the new server

On the new server we installed GitLab using the same settings as on the old
server by copying over our Chef attributes and using cookbook-gitlab. Because
we were using an external (RDS) database instance, we did not have to worry
about migrating that data as well.

Once the DRBD drives are in sync (look for `ds:UpToDate/UpToDate` in the output
of `cat /proc/drbd`) we can schedule the second maintenance window to 'fail
over' to the new server.

This is the procedure we followed during the maintenance window to fail over to
the new server, once the new server was fully set up.

```
# OLD

# Stop GitLab.
sudo service gitlab stop

# Unmount the filesystem with the git data.
sudo umount /mnt/ebs

# Take snapshot of EBS volume.
# Take snapshot of RDS instance.

# Check whether DRBD is in sync; look for `ds:UpToDate/UpToDate`.
cat /proc/drbd 

# Shut down DRBD synchronization.
sudo drbdadm down gitlab_data

# NEW

# Shut down DRBD synchronization.
sudo drbdadm down gitlab_data

# Rename (remove) the DRBD config to prevent any future accidents.
sudo mv /etc/drbd.d/gitlab_data.res{,.old}

# Mount the new volume directly, bypassing DRBD.
# Note that we are using the same mountpoint as we did on the old server.
sudo mount /dev/vg0/gitlab_data /mnt/ebs

# Online resize the filesystem (the reason we are migrating in the first place)
sudo resize2fs /dev/vg0/gitlab_data 

# In our case we needed to rebuild /home/git/.ssh/authorized_keys
cd /home/git/gitlab && sudo -u git -H bundle exec rake gitlab:shell:setup RAILS_ENV=production

# Start GitLab
sudo service gitlab start

# Reassign the AWS Elastic IP to redirect all traffic to the new server
```

## Conclusion

Thanks to DRBD we were able to migrate all of our data to the new server with
minimal downtime for our users. This came at the expense of a very complex
procedure, however. Our recommendation to administrators who expect to store a
large amount of repository data in GitLab is to move the repositories and
satellite data to an LVM volume using the Rsync approach. Once you are on an
LVM volume you can keep growing for a long time by simply adding disks.
