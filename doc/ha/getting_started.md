# Getting started with GitLab and High Availability

By 'High Availability' we mean the ability to recover from application or
server failure quickly and with minimal or no data loss.

Note: throughout this document we assume that your GitLab server uses
our omnibus packages.

## Choosing the right GitLab HA configuration for your team and environment

The high availability solutions for GitLab we discuss in this documents rely on
third-party solutions such as DRBD, storage appliances, database clusters and
load balancers. To achieve optimal availabilty it is crucial to not only match
the solutions to GitLab, but also to your operational environment.

If you have any questions or if you need assistance with your setup please
contact us at subscribers@gitlab.com.

## GitLab architecture

A GitLab installation acts as a front-end for Git repositories stored on a
filesystem. End users interact with the GitLab server via NGINX or Apache (via
web browsers and Git clients) and the SSH daemon running on your server (via
Git clients).

NGINX acts as a reverse proxy to Unicorn, a Ruby web server. Unicorn manages the
database of GitLab users and repository metadata in a Postgres (or MySQL)
database. In some cases, Unicorn will hand off a user request to the Sidekiq
background processing daemon for asynchronous processing, using Redis as a
message bus. A typical task performed by Sidekiq is to send emails, either via
a local Sendmail-compatible server or via a remote SMTP server.

Incoming SSH connections from Git clients get handled by gitlab-shell, which
acts as a gatekeeper for the Git repositories stored on disk. Gitlab-shell
queries Unicorn via an HTTP API to find out if the user has access to a given
repository. In case a Git client pushes new commits, gitlab-shell will also
notify Sidekiq about this via Redis.

In a standard omnibus-gitlab installation, the NGINX, Unicorn, Sidekiq, Redis
and PostgreSQL processes are all managed and supervised by omnibus-gitlab using
Runit. Gitlab-shell only creates short-lived processes which mostly get managed
by the SSH daemon.

> Ensuring the availability of the Sendmail (Postfix) or SMTP service and
of the SSH daemon is left to the system administator.

## Application failure

The built-in Runit supervision tree of will restart any of the services managed
by GitLab (NGINX, Unicorn, Sidekiq, Redis and PostgreSQL) if necessary.

> If you are using Apache or MySQL, or your own NGINX service, then Runit
 cannot manage those services for you.

Runit does not protect against runaway processes consuming too much CPU or RAM;
we recommend you to set up standard monitoring tools for your GitLab server.
Also see
https://gitlab.com/standard/high-availability/blob/master/doc/monitoring/processes.md
.

## Server failure

GitLab stores its vital data in three places: on disk (Git repositories), in
PostgreSQL/MySQL (user and project metadata) and in Redis (user sessions and
inter-process messages). To protect against server failure we must make sure
that we can make these three data sources available on a second GitLab server.

We currently support two strategies to make this happen.

- Storing _all_ GitLab data on a highly available local filesystem (backed by
  DRBD or a SAN);
- Storing the data 'outside' the GitLab server so that GitLab can access it via
  the network.

The DRBD approach has fewer moving parts (only two servers) but it only allows
for an active/passive configuration.

The 'store data outside' approach has more moving parts (GitLab server, NFS
server, Redis server, PostgreSQL/MySQL server) but it has the advantage that
you can create a load-balanced active/active cluster with multiple GitLab
nodes.

For GitLab.com we currently (September 2014) use DRBD combined with a floating
IP address to create manual failover capability. Our tooling for DRBD is
available in https://gitlab.com/standard/gitlab-drbd .

If you want to create a load-balanced cluster please see our tips in
https://gitlab.com/standard/high-availability/blob/master/doc/cluster/load-balanced_cluster.md
.
